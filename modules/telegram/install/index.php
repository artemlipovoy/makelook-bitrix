<?php

use Bitrix\Main\EventManager;

class telegram extends CModule
{
    var $MODULE_ID = "telegram";
    var $MODULE_VERSION;
    var $MODULE_VERSION_DATE;
    var $MODULE_NAME;
    var $MODULE_DESCRIPTION;

    function telegram()
    {
        $arModuleVersion = array();

        $path = str_replace("\\", "/", __FILE__);
        $path = substr($path, 0, strlen($path) - strlen("/index.php"));
        include($path . "/version.php");

        if (is_array($arModuleVersion) && array_key_exists("VERSION", $arModuleVersion)) {
            $this->MODULE_VERSION = $arModuleVersion["VERSION"];
            $this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
        }
        $this->MODULE_NAME = "Интеграция с Telegram";
        $this->MODULE_DESCRIPTION = "";
        $this->PARTNER_NAME = "MakeLook";
        $this->PARTNER_URI = "";
    }

    function DoInstall()
    {
        $this->InstallEvents();
        $this->InstallFiles();
        $this->InstallFields();
        $this->createUserHLB();
        RegisterModule($this->MODULE_ID);
        return true;
    }

    function DoUninstall()
    {

        $this->UnInstallEvents();
        $this->UnInstallFiles();
        $this->UnInstallFields();
        $this->deleteUserHLB();
        UnRegisterModule($this->MODULE_ID);
        return true;
    }

    function InstallEvents()
    {

        return true;
    }

    function UnInstallEvents()
    {

        return true;
    }

    function InstallFields(){

        return true;
    }

    function UnInstallFields(){

        return true;
    }

    function InstallFiles()
    {

        return true;
    }

    function UnInstallFiles()
    {

        return true;
    }

    function createUserHLB(){
        \Bitrix\Main\Loader::IncludeModule('highloadblock');

        $result = \Bitrix\Highloadblock\HighloadBlockTable::add([
            'NAME' => 'TelegramUserBindings',
            'TABLE_NAME' => 'telegram_user_bindings',
        ]);
        $id = $result->getId();
        if(!$id)
            return false;

        \Bitrix\Highloadblock\HighloadBlockLangTable::add(array(
            'ID' => $id,
            'LID' => 'ru',
            'NAME' => 'Привязки Telegram'
        ));

        $hlEntityID = 'HLBLOCK_'.$id;
        $arFields = [
            'UF_TG_DISPLAY_NAME'=>Array(
                'ENTITY_ID' => $hlEntityID,
                'FIELD_NAME' => 'UF_TG_DISPLAY_NAME',
                'USER_TYPE_ID' => 'string',
                'MANDATORY' => 'Y',
                "EDIT_FORM_LABEL" => Array('ru'=>'Имя в Телеграме'),
                "LIST_COLUMN_LABEL" => Array('ru'=>'Имя в Телеграме'),
                "LIST_FILTER_LABEL" => Array('ru'=>'Имя в Телеграме'),
                "ERROR_MESSAGE" => Array('ru'=>'', 'en'=>''),
                "HELP_MESSAGE" => Array('ru'=>'', 'en'=>''),
            ),
            'UF_TG_USER_ID'=>Array(
                'ENTITY_ID' => $hlEntityID,
                'FIELD_NAME' => 'UF_TG_USER_ID',
                'USER_TYPE_ID' => 'string',
                'MANDATORY' => 'Y',
                "EDIT_FORM_LABEL" => Array('ru'=>'ID в Телеграме'),
                "LIST_COLUMN_LABEL" => Array('ru'=>'ID в Телеграме'),
                "LIST_FILTER_LABEL" => Array('ru'=>'ID в Телеграме'),
                "ERROR_MESSAGE" => Array('ru'=>'', 'en'=>''),
                "HELP_MESSAGE" => Array('ru'=>'', 'en'=>''),
            ),
            'UF_B24_USER_ID'=>Array(
                'ENTITY_ID' => $hlEntityID,
                'FIELD_NAME' => 'UF_B24_USER_ID',
                'USER_TYPE_ID' => 'string',
                'MANDATORY' => '',
                "EDIT_FORM_LABEL" => Array('ru'=>'ID в Б24'),
                "LIST_COLUMN_LABEL" => Array('ru'=>'ID в Б24'),
                "LIST_FILTER_LABEL" => Array('ru'=>'ID в Б24'),
                "ERROR_MESSAGE" => Array('ru'=>'', 'en'=>''),
                "HELP_MESSAGE" => Array('ru'=>'', 'en'=>''),
            ),
        ];
        foreach($arFields as $arCartField){
            $obUserField  = new \CUserTypeEntity;
            $obUserField->Add($arCartField);
        }
    }

    function deleteUserHLB(){
        \Bitrix\Main\Loader::IncludeModule('highloadblock');


        $hlid = \Bitrix\Highloadblock\HighloadBlockTable::getList([
            'filter'=>[
                'NAME'=>'TelegramUserBindings'
            ],
            'select'=>[
                'ID'
            ]
        ])->fetch()['ID'];
        if($hlid)
            \Bitrix\Highloadblock\HighloadBlockTable::delete($hlid);
    }
}
