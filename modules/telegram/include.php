<?php

Bitrix\Main\Loader::registerAutoloadClasses(
    "telegram",
    array(
        '\MakeLook\Telegram\UserHighload' => '/lib/UserHighload.php',
        '\MakeLook\Telegram\Connector' => '/lib/Connector.php'
    )
);