<?php


namespace MakeLook\Telegram;


class Connector
{
    public static function sendMessage($userId, $text){
        $userHighload = UserHighload::get();
        $chatId = $userHighload::getList([
            'filter'=>[
                'UF_B24_USER_ID'=>$userId
            ],
            'select'=>[
                'UF_TG_USER_ID'
            ]
        ])->fetch()['UF_TG_USER_ID'];

        if(!$chatId)
            return false;

        $response = self::callTelegram(
            'sendMessage',
            array(
                'chat_id' => $chatId,
                'text' => $text,
                'parse_mode' => 'markdown'
            )
        );

        return json_decode($response, true);
    }

    private static function callTelegram($method, $request){
        $token = \Bitrix\Main\Config\Option::get('telegram', 'BotToken');
        if(!$token)
            return false;

        $ch = curl_init('https://api.telegram.org/bot' . $token . '/' . $method);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $request);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, false);
        $response = curl_exec($ch);
        curl_close($ch);

        return $response;
    }
}