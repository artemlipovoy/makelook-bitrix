<?php


namespace MakeLook\Telegram;


class UserHighload
{
    public static function get(){
        \Bitrix\Main\Loader::IncludeModule('highloadblock');
        $hlid = \Bitrix\Highloadblock\HighloadBlockTable::getList([
            'filter'=>[
                'NAME'=>'TelegramUserBindings'
            ],
            'select'=>[
                'ID'
            ]
        ])->fetch()['ID'];

        $hlb = \Bitrix\Highloadblock\HighloadBlockTable::getById($hlid)->fetch();
        $entity = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlb);
        return $entity->getDataClass();
    }
}