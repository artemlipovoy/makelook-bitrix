<?php
define("NOT_CHECK_PERMISSIONS", true);
define("NEED_AUTH", false);
define("NO_KEEP_STATISTIC", true);

require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

if(!\Bitrix\Main\Loader::includeModule('telegram'))
    die();

$token = Bitrix\Main\Config\Option::get('telegram', 'BotToken');
if(!$token)
    die();

$data = json_decode(file_get_contents('php://input'), true);
$userHighload = \MakeLook\Telegram\UserHighload::get();

$from = $data['message']['from'];

$res = $userHighload::getList([
    'filter'=>[
        'UF_TG_USER_ID'=>$from['id']
    ],
])->fetch();

$displayName = implode(' ',[$from['first_name'],'('.$from['username'].')']);
if(!$res){
    $userHighload::add([
        'UF_TG_USER_ID'=>$from['id'],
        'UF_TG_DISPLAY_NAME' => $displayName
    ]);
}else{
    if($res['UF_TG_DISPLAY_NAME']!=$displayName){
        $userHighload::update($res['ID'],[
            'UF_TG_DISPLAY_NAME'=>$displayName
        ]);
    }
}


