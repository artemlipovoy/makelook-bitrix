<?php /** @noinspection ALL */

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\HttpApplication;
use Bitrix\Main\Loader;
use Bitrix\Main\Config\Option;
use Bitrix\Main\EventManager;
use Bitrix\Main\UserTable;

Loc::loadMessages(__FILE__);

$request = HttpApplication::getInstance()->getContext()->getRequest();
$module_id = htmlspecialcharsbx($request["mid"] != "" ? $request["mid"] : $request["id"]);
Loader::includeModule($module_id);


$res = UserTable::getList([
    'filter'=>[
        'ACTIVE'=>'Y',
        '!%LOGIN'=>'imopenlines'
    ],
    'select'=>[
        'NAME',
        'LAST_NAME',
        'ID',
        'LOGIN'
    ]
]);
$users = [];
while($ob = $res->fetch()){
    $users[$ob['ID']] = trim(implode(' ',[$ob['NAME'], $ob['LAST_NAME']]))?:$ob['LOGIN'];
}

$options = [
    "Основные",
    [
        "BotToken",
        "Токен",
        "",
        ["text", 50]
    ],
    "Пользователи бота",
];
$userHighload = \MakeLook\Telegram\UserHighload::get();
$res = $userHighload::getList();
while($ob = $res->fetch()){
    $options[] = [
        'tguser_'.$ob['ID'],
        $ob['UF_TG_DISPLAY_NAME'],
        $ob['UF_B24_USER_ID'],
        ["text", 50]
    ];
}

$aTabs = [
    [
        "DIV" => "edit",
        "TAB" => "Настройки",
        "TITLE" => "Настройки",
        "OPTIONS" => $options
    ]
];

if ($request->isPost() && check_bitrix_sessid()) {
    foreach ($aTabs as $aTab) {
        foreach ($aTab["OPTIONS"] as $arOption) {
            if (!is_array($arOption)) {
                continue;
            }
            if ($request["apply"]) {
                $optionValue = $request->getPost($arOption[0]);
                $splt = explode('_', $arOption[0]);
                if($arOption[0]=="BotToken"){
                    file_get_contents("https://api.telegram.org/bot".$optionValue."/setWebhook?url=https://makelook.ru/local/modules/telegram/tools/bot.php");
                }
                if($splt[0]=='tguser') {
                    $userHighload::update($splt[1], [
                        'UF_B24_USER_ID' => $optionValue
                    ]);
                }else {
                    Option::set($module_id, $arOption[0], is_array($optionValue) ? implode(",", $optionValue) : $optionValue);
                }
            }
        }
    }

    /** @var $APPLICATION */
    LocalRedirect($APPLICATION->GetCurPage() . "?mid=" . $module_id . "&lang=" . LANG);
}

$tabControl = new CAdminTabControl(
    "tabControl",
    $aTabs
);

$tabControl->Begin();
?>
    <form action="<?php echo ($APPLICATION->GetCurPage()); ?>?mid=<?php echo ($module_id); ?>&lang=ru" method="post">
        <?php
        foreach ($aTabs as $k=>$aTab) {
            if ($aTab["OPTIONS"]) {
                $tabControl->BeginNextTab();
                __AdmSettingsDrawList($module_id, $aTab["OPTIONS"]);
            }
        }
        $tabControl->Buttons();
        ?>
        <input type="submit" name="apply" value="Сохранить" class="adm-btn-save" />
        <?php
        echo (bitrix_sessid_post());
        ?>
    </form>
<?php
$tabControl->End();
?>