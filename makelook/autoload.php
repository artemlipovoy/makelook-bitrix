<?php

use Bitrix\Main\Diag\Debug;
use Bitrix\Main\Loader;
use Bitrix\Main\LoaderException;

try {
    Loader::registerAutoloadClasses(null, [
        //services
        "MakeLook\Rest\Services\Bitrix\CRest"            =>"/local/makelook/rest/services/bitrix/rest/CRest.php",
        "MakeLook\Rest\Services\Bitrix\Entities\CrmDeal" => "/local/makelook/rest/services/bitrix/entities/CrmDeal.php",
        "MakeLook\Rest\Services\Bitrix\Entities\CrmContact" => "/local/makelook/rest/services/bitrix/entities/CrmContact.php",
        "MakeLook\Rest\Services\Bitrix\Entities\CrmProduct" => "/local/makelook/rest/services/bitrix/entities/CrmProduct.php",

        //base interface for read
        "MakeLook\Rest\EntityInterface"                  => "/local/makelook/rest/EntityInterface.php",

        //entities tables
        "MakeLook\Entities\Base"                         => "/local/makelook/entities/Base.php",
        "MakeLook\Entities\ModelsTable"                  => "/local/makelook/entities/ModelsTable.php",
        "MakeLook\Entities\BrandsTable"                  => "/local/makelook/entities/BrandsTable.php",
        "MakeLook\Entities\ColorsTable"                  => "/local/makelook/entities/ColorsTable.php",
        "MakeLook\Entities\Clothes\SizesTable"           => "/local/makelook/entities/clothes/SizesTable.php",
        "MakeLook\Entities\Clothes\CompositionTable"     => "/local/makelook/entities/clothes/CompositionTable.php",
        "MakeLook\Entities\Clothes\CategoriesTable"      => "/local/makelook/entities/clothes/CategoriesTable.php",
        "MakeLook\Entities\Clothes\ClothesTable"         => "/local/makelook/entities/clothes/ClothesTable.php",
        "MakeLook\Entities\Clothes\PhotosTable"          => "/local/makelook/entities/clothes/PhotosTable.php",

        //rest classes
        "MakeLook\Rest\Controllers\Base"                 => "/local/makelook/rest/controllers/Base.php",
        "MakeLook\Rest\Controllers\Clothes"              => "/local/makelook/rest/controllers/Clothes.php",
        "MakeLook\Rest\Controllers\Collections"          => "/local/makelook/rest/controllers/Collections.php",
        "MakeLook\Rest\Controllers\Integration\Bitrix24" => "/local/makelook/rest/controllers/integration/Bitrix24.php",

        //rest generator
        "MakeLook\Rest\Generator"                        => "/local/makelook/rest/Generator.php"
    ]);

    MakeLook\Rest\Generator::bind();

} catch (LoaderException $e) {
    Debug::writeToFile($e,"",'/local/errlog.log');
}