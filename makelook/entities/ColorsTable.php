<?php


namespace MakeLook\Entities;


class ColorsTable extends Base
{
    const ID = 4;

    public static function getList($params=[]){
        return self::getEntity(self::ID)::getList($params);
    }

    public static function getById($id){
        return self::getEntity(self::ID)::getById($id);
    }

    public static function getMap(){
        return array (
            'ID' =>
                array (
                    'type' => 'primary',
                ),
            'UF_NAME' =>
                array (
                    'type' => 'text',
                ),
            'UF_HEX' =>
                array (
                    'type' => 'text',
                ),
        );
    }
    public static function getRestAliases(){
        return array (
            'ID' => 'ID',
            'name' => 'UF_NAME',
            'hex' => 'UF_HEX'
        );
    }
}
