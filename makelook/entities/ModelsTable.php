<?php


namespace MakeLook\Entities;


class ModelsTable extends Base
{
    const ID = 1;

    public static function getList($params=[]){
        return self::getEntity(self::ID)::getList($params);
    }

    public static function getById($id){
        return self::getEntity(self::ID)::getById($id);
    }

    public static function getMap(){
        return array (
            'ID' =>
                array (
                    'type' => 'primary',
                ),
            'UF_NAME' =>
                array (
                    'type' => 'text',
                ),
            'UF_HEIGHT' =>
                array (
                    'type' => 'double',
                ),
            'UF_CHEST' =>
                array (
                    'type' => 'double',
                ),
            'UF_WAIST' =>
                array (
                    'type' => 'double',
                ),
            'UF_HIPS' =>
                array (
                    'type' => 'double',
                ),
            'UF_FOOT_SIZE' =>
                array (
                    'type' => 'double',
                ),
        );
    }
    public static function getRestAliases(){
        return array (
            'ID' => 'ID',
            'name' => 'UF_NAME',
            'height' => 'UF_HEIGHT',
            'chest' => 'UF_CHEST',
            'waist' => 'UF_WAIST',
            'hips' => 'UF_HIPS',
            'foot_size' => 'UF_FOOT_SIZE',
        );
    }
}