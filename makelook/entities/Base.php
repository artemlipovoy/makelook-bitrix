<?php


namespace MakeLook\Entities;


use Bitrix\Highloadblock\HighloadBlockTable;
use Bitrix\Main\Loader;

class Base
{
    const FIELD_FILE = 'file';
    const FIELD_IMAGE = 'image';
    const FIELD_ENTITY = 'entity';
    const FIELD_LIST = 'list';
    const FIELD_FUNCTION = 'func';

    public static function getEntity($id){
        Loader::includeModule('highloadblock');
        $hlblock = HighloadBlockTable::getById($id)->fetch();
        $entity = HighloadBlockTable::compileEntity($hlblock);
        return $entity->getDataClass();
    }
}