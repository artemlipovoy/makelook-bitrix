<?php


namespace MakeLook\Entities\Clothes;

use MakeLook\Entities\Base;


class CategoriesTable extends Base
{
    const ID = 6;

    public static function getList($params=[]){
        return self::getEntity(self::ID)::getList($params);
    }

    public static function getById($id){
        return self::getEntity(self::ID)::getById($id);
    }

    public static function getMap(){
        return array (
            'ID' =>
                array (
                    'type' => 'primary',
                ),
            'UF_NAME' =>
                array (
                    'type' => 'text',
                ),
            'UF_NAME_SINGLE' =>
                array (
                    'type' => 'text',
                ),
            'UF_TYPE' =>
                array (
                    'type' => 'text',
                ),
            'UF_SUBCATEGORIES' =>
                array (
                    'type' => self::FIELD_ENTITY,
                    'class' => "\MakeLook\Entities\Clothes\CategoriesTable"
                ),
        );
    }
    public static function getRestAliases(){
        return array (
            'ID' => 'ID',
            'name' => 'UF_NAME',
            'subcategories' => 'UF_SUBCATEGORIES',
            'name_single' => 'UF_NAME_SINGLE',
            'type' => 'UF_TYPE'
        );
    }
}