<?php


namespace MakeLook\Entities\Clothes;

use MakeLook\Entities\Base;


class ClothesTable extends Base
{
    const ID = 7;

    public static function getList($params=[]){
        return self::getEntity(self::ID)::getList($params);
    }

    public static function getById($id){
        return self::getEntity(self::ID)::getById($id);
    }

    public static function getAvailableSizes($result){
        $res = PhotosTable::getList([
            'filter'=>[
                'UF_CLOTHES'=>$result['ID'],
            ],
            'select'=>[
                'UF_SIZE'
            ]
        ]);
        while($ob = $res->fetch()){
            $ids[] = $ob['UF_SIZE'];
        }
        array_unique($ids);
        return SizesTable::getList([
            'filter'=>[
                'ID'=>$ids
            ],
            'select'=>SizesTable::getRestAliases()
        ])->fetchAll();
    }

    public static function getMap(){
        return array (
            'ID' =>
                array (
                    'type' => 'primary',
                ),
            'UF_NAME' =>
                array (
                    'type' => 'text',
                ),
            'UF_PRICE' =>
                array (
                    'type' => 'text',
                ),
            'UF_CATEGORY' =>
                array (
                    'type' => self::FIELD_ENTITY,
                    'class' => "\MakeLook\Entities\Clothes\CategoriesTable"
                ),
            'UF_BRAND' =>
                array (
                    'type' => self::FIELD_ENTITY,
                    'class' => "\MakeLook\Entities\BrandsTable"
                ),
            'UF_COLOR' =>
                array (
                    'type' => self::FIELD_ENTITY,
                    'class' => "\MakeLook\Entities\ColorsTable"
                ),
            'UF_CLOTHES_COMPOSITION' =>
                array (
                    'type' => self::FIELD_ENTITY,
                    'class' => "\MakeLook\Entities\Clothes\CompositionTable"
                ),
            'UF_SIZE' =>
                array (
                    'type' => self::FIELD_ENTITY,
                    'class' => "\MakeLook\Entities\Clothes\SizesTable"
                ),
            'UF_PHOTO' =>
                array (
                    'type' => self::FIELD_IMAGE,
                ),
            'UF_TYPE' =>
                array (
                    'type' => self::FIELD_LIST,
                    'values' => [
                        4=>"shoes",
                        3=>"clothes"
                    ]
                ),
            'UF_AVAILABLE_SIZES' =>
                array (
                    'type' => self::FIELD_FUNCTION,
                    'function' => 'getAvailableSizes'
                )
        );
    }
    public static function getRestAliases(){
        return array (
            'ID' => 'ID',
            'name' => 'UF_NAME',
            'category' => 'UF_CATEGORY',
            'brand' => 'UF_BRAND',
            'color' => 'UF_COLOR',
            'clothes_composition' => 'UF_CLOTHES_COMPOSITION',
            'size' => 'UF_SIZE',
            'photo' => 'UF_PHOTO',
            'price' => 'UF_PRICE',
            'type' => 'UF_TYPE',
            'available_sizes'=>'UF_AVAILABLE_SIZES'
        );
    }
}
