<?php


namespace MakeLook\Entities\Clothes;

use MakeLook\Entities\Base;
use MakeLook\Rest\EntityInterface;
use Prominado\Rest\Request;


class PhotosTable extends Base
{
    const ID = 8;

    public static function getList($params=[]){
        return self::getEntity(self::ID)::getList($params);
    }

    public static function getById($id){
        return self::getEntity(self::ID)::getById($id);
    }

    public static function getAvailableSizes($result){
        $res = self::getList([
            'filter'=>[
                'UF_CLOTHES'=>$result['clothes'][0]['ID'],
                'UF_MODEL'=>$result['model'][0]['ID']
            ],
            'select'=>[
                'UF_SIZE'
            ]
        ]);
        while($ob = $res->fetch()){
            $ids[] = $ob['UF_SIZE'];
        }
        return SizesTable::getList([
            'filter'=>[
                'ID'=>$ids
            ],
            'select'=>SizesTable::getRestAliases()
        ])->fetchAll();
    }

    public static function getMap(){
        return array (
            'ID' =>
                array (
                    'type' => 'primary',
                ),
            'UF_CLOTHES' =>
                array (
                    'type' => self::FIELD_ENTITY,
                    'class' => "\MakeLook\Entities\Clothes\ClothesTable"
                ),
            'UF_SIZE' =>
                array (
                    'type' => self::FIELD_ENTITY,
                    'class' => "\MakeLook\Entities\Clothes\SizesTable"
                ),
            'UF_MODEL' =>
                array (
                    'type' => self::FIELD_ENTITY,
                    'class' => "\MakeLook\Entities\ModelsTable"
                ),
            'UF_SPRITES' =>
                array (
                    'type' => self::FIELD_IMAGE,
                ),
            'UF_FIRST_SPRITE' =>
                array (
                    'type' => self::FIELD_FILE,
                ),
            'UF_LAYER' =>
                array (
                    'type' => 'text',
                ),
            'UF_AVAILABLE_SIZES' =>
                array (
                    'type' => self::FIELD_FUNCTION,
                    'function' => 'getAvailableSizes'
                )
        );
    }
    public static function getRestAliases(){
        return array (
            'ID' => 'ID',
            'clothes' => 'UF_CLOTHES',
            'size' => 'UF_SIZE',
            'available_sizes' => 'UF_AVAILABLE_SIZES',
            'sprites' => 'UF_SPRITES',
            'model' => 'UF_MODEL',
            'layer' => 'UF_LAYER',
        );
    }
}