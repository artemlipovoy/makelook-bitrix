<?php


namespace MakeLook\Entities\Clothes;

use MakeLook\Entities\Base;

class SizesTable extends Base
{
    const ID = 3;

    public static function getList($params=[]){
        return self::getEntity(self::ID)::getList($params);
    }

    public static function getById($id){
        return self::getEntity(self::ID)::getById($id);
    }

    public static function getMap(){
        return array (
            'ID' =>
                array (
                    'type' => 'primary',
                ),
            'UF_SIZE' =>
                array (
                    'type' => 'text',
                ),
            'UF_TYPE' =>
                array (
                    'type' => self::FIELD_LIST,
                    'values' => [
                        1=>"shoes",
                        2=>"clothes"
                    ]
                ),
        );
    }
    public static function getRestAliases(){
        return array (
            'ID' => 'ID',
            'size' => 'UF_SIZE',
            'type' => 'UF_TYPE'
        );
    }
}