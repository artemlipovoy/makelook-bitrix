<?php


namespace MakeLook\Entities\Clothes;

use MakeLook\Entities\Base;


class CompositionTable extends Base
{
    const ID = 5;

    public static function getList($params=[]){
        return self::getEntity(self::ID)::getList($params);
    }

    public static function getById($id){
        return self::getEntity(self::ID)::getById($id);
    }

    public static function getMap(){
        return array (
            'ID' =>
                array (
                    'type' => 'primary',
                ),
            'UF_NAME' =>
                array (
                    'type' => 'text',
                ),
        );
    }
    public static function getRestAliases(){
        return array (
            'ID' => 'ID',
            'name' => 'UF_NAME',
        );
    }
}