<?php


namespace MakeLook\Rest\Services\Bitrix\Entities;


use MakeLook\Rest\Services\Bitrix\CRest;

class CrmProduct
{
    const TYPE_PIECE = 9;
    /**
     * @param $fields
     * @return bool|mixed|string
     */
    public static function add($fields){
        if($fields['XML_ID']){
            $response = CRest::call('crm.product.list',[
                'filter'=>[
                    'XML_ID' => $fields['XML_ID']
                ],
                'select'=>['ID']
            ])['result'][0];
            if($id = $response['ID']){
                return $id;
            }
        }
        $response = CRest::call('crm.product.add',[
            'fields'=>$fields
        ]);
        return $response;
//        ])['result'];
//        if($id = $response){
//            return $id;
//        }
//        return false;
    }
}