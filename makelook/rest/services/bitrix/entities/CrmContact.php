<?php


namespace MakeLook\Rest\Services\Bitrix\Entities;


use MakeLook\Rest\Services\Bitrix\CRest;

class CrmContact
{
    /**
     * @param $fields
     * @return bool|mixed|string
     */
    public static function add($fields){
        $response = CRest::call("crm.contact.add", [
            'fields'=>$fields,
            'params'=>[
                "REGISTER_SONET_EVENT"=> "Y"
            ]
        ])['result'];
        if($id = $response){
            return $id;
        }
        return false;
    }
}