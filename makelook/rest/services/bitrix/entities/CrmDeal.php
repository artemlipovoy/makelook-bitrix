<?php


namespace MakeLook\Rest\Services\Bitrix\Entities;


use MakeLook\Rest\Services\Bitrix\CRest;

class CrmDeal
{
    const MAPPING = [
        'PRODUCT_LINKS'=>'UF_CRM_1607649609'
    ];

    /**
     * @param $fields
     * @return bool|mixed|string
     */
    public static function add($fields){
        $response = CRest::call("crm.deal.add", [
            'fields'=>$fields,
            'params'=>[
                "REGISTER_SONET_EVENT"=> "Y"
            ]
        ])['result'];
        if($id = $response){
            return $id;
        }
        return false;
    }
}