<?php


namespace MakeLook\Rest\Controllers;


class Base
{
    function getMap(){
        $methods = get_class_methods($this);

        $methods = array_diff($methods,['__construct', 'getMap']);

        $shortClassname = (new \ReflectionClass($this))->getShortName();
        $restMap = [];
        foreach($methods as $method){
            $restMap[strtolower($shortClassname.'.'.$method)] = [
                'allow_methods'=>['GET', 'POST'],
                'callback'=>[(new \ReflectionClass($this))->getName(), $method],
            ];
        }
        return $restMap;
    }
}