<?php


namespace MakeLook\Rest\Controllers;


use MakeLook\Entities\Clothes\PhotosTable;
use MakeLook\Rest\EntityInterface;
use Prominado\Rest\Request;

class Clothes extends Base
{
    public function getRandomLook(Request $request){
        if($modelId = $request->getQuery('model')){
            $allClothes = PhotosTable::getList([
                'filter'=>[
                    'UF_MODEL'=>$modelId
                ]
            ])->fetchAll();
        }else{
            $allClothes = PhotosTable::getList()->fetchAll();
        }

        shuffle($allClothes);
        $arIds = [];
        $modelId = null;
        foreach($allClothes as $clothes){
            if(!$modelId){
                $modelId = $clothes['UF_MODEL'];
            }
            if(in_array(array_keys($arIds),$clothes['UF_LAYER']) or $modelId!=$clothes['UF_MODEL'])
                continue;
            $arIds[$clothes['UF_LAYER']] = $clothes['ID'];
        }
        return array_values($arIds);
    }

    public function getRandomClothes(Request $request){
        $layer = $request->getQuery('layer');
        $model = $request->getQuery('model');
        $clothes = PhotosTable::getList([
            'filter'=>[
                'UF_LAYER'=>$layer,
                'UF_MODEL'=>$model
            ],
            'select'=>['ID']
        ])->fetchAll();
        shuffle($clothes);

        $req = new Request();
        $req->setQuery([
            'id'=>array_pop($clothes),
            '__class'=>'\MakeLook\Entities\Clothes\PhotosTable'
        ]);
        return EntityInterface::restGetById($req);
    }

    public function loadForAnotherModel(Request $request){
        $photosElements = $request->getQuery('elements');
        $modelId = $request->getQuery('modelId');

        $res = PhotosTable::getList(['filter'=>['ID'=>$photosElements]]);

        $clothesIds = [];
        $clothesToPhotosRelation = [];
        $arPhotos = [];
        while($ob = $res->fetch()){
            $arPhotos[$ob['UF_CLOTHES']] = $ob;
            $clothesIds[] = $ob['UF_CLOTHES'];
            $clothesToPhotosRelation[$ob['UF_CLOTHES']] = $ob['ID'];
        }

        $res = PhotosTable::getList([
            'filter'=>[
                'UF_MODEL'=>$modelId,
                'UF_CLOTHES'=>$clothesIds
            ],
            'select'=>[
                'ID',
                'UF_CLOTHES'
            ]
        ]);
        $arResultClothes=[];
        $result = [];
        while($ob = $res->fetch()){
            if(in_array($ob['UF_CLOTHES'], $arResultClothes))
                continue;
            $arResultClothes[] = $ob['UF_CLOTHES'];
            $result[] = $ob['ID'];
        }
        $diff = array_values(array_diff(array_keys($clothesToPhotosRelation), $arResultClothes));
        if(!$diff){
            return [
                'withNew'=>false,
                'newIds'=>$result
            ];
        }
        foreach($diff as $id){
            $layer = $arPhotos[$id]['UF_LAYER'];
            $newClothes = PhotosTable::getList([
                'filter'=>[
                    'UF_LAYER'=>$layer,
                    'UF_MODEL'=>$modelId
                ],
                'select'=>['ID']
            ])->fetch()['ID'];
            $result[] = $newClothes;
        }
        return [
            'withNew'=>true,
            'newIds'=>$result
        ];
    }

    public function insertInLook1(Request $request){
        $clothesId = $request->getQuery('clothes');
        $layer = $request->getQuery('layer');
        $model = $request->getQuery('model');

        $clothesPhoto = PhotosTable::getList([
            'filter'=>[
                'UF_CLOTHES'=>$clothesId,
                'UF_LAYER'=>$layer,
                'UF_MODEL'=>$model
            ],
            'select'=>['ID']
        ])->fetch()['ID'];

        if($clothesPhoto){
            $req = new Request();
            $req->setQuery([
                'id'=>$clothesPhoto,
                '__class'=>"\MakeLook\Entities\Clothes\PhotosTable",
            ]);
            $clothesPhotoData = EntityInterface::restGetById($req);
            return [
                'newLook'=>false,
                'photoData'=>$clothesPhotoData
            ];
        }

        $clothesPhotoData = PhotosTable::getList([
            'filter'=>[
                'UF_CLOTHES'=>$clothesId,
                'UF_LAYER'=>$layer,
            ],
            'select'=>['ID','UF_MODEL']
        ])->fetch();

        $clothesPhoto = $clothesPhotoData['ID'];
        $model = $clothesPhotoData['UF_MODEL'];

        $allClothes = PhotosTable::getList([
            'filter'=>[
                'UF_MODEL'=>$model
            ]
        ])->fetchAll();

        shuffle($allClothes);
        $arIds = [];
        foreach($allClothes as $clothes){
            if(in_array(array_keys($arIds),$clothes['UF_LAYER']))
                continue;
            $arIds[$clothes['UF_LAYER']] = $clothes['ID'];
        }
        $arIds[$layer] = $clothesPhoto;

        return [
            'newLook'=>true,
            'photoIds'=>array_values($arIds),
            'newModelId'=>$model
        ];
    }

    public function insertInLook(Request $request){
        $clothesId = $request->getQuery('clothes');
        $model = $request->getQuery('model');

        $clothesPhotoData = PhotosTable::getList([
            'filter'=>[
                'UF_CLOTHES'=>$clothesId,
                'UF_MODEL'=>$model
            ],
            'select'=>['ID','UF_LAYER']
        ])->fetch();

        $clothesPhoto = $clothesPhotoData['ID'];
        $layer = $clothesPhotoData['UF_LAYER'];

        if($clothesPhoto){
            $req = new Request();
            $req->setQuery([
                'id'=>$clothesPhoto,
                '__class'=>"\MakeLook\Entities\Clothes\PhotosTable",
            ]);
            $clothesPhotoData = EntityInterface::restGetById($req);
            return [
                'newLook'=>false,
                'photoData'=>$clothesPhotoData,
                'layer'=>$layer
            ];
        }

        $clothesPhotoData = PhotosTable::getList([
            'filter'=>[
                'UF_CLOTHES'=>$clothesId,
            ],
            'select'=>['ID','UF_MODEL','UF_LAYER']
        ])->fetch();


        $layer = $clothesPhotoData['UF_LAYER'];
        $clothesPhoto = $clothesPhotoData['ID'];
        $model = $clothesPhotoData['UF_MODEL'];

        $allClothes = PhotosTable::getList([
            'filter'=>[
                'UF_MODEL'=>$model
            ]
        ])->fetchAll();

        shuffle($allClothes);
        $arIds = [];
        foreach($allClothes as $clothes){
            if(in_array(array_keys($arIds),$clothes['UF_LAYER']))
                continue;
            $arIds[$clothes['UF_LAYER']] = $clothes['ID'];
        }
        $arIds[$layer] = $clothesPhoto;

        return [
            'newLook'=>true,
            'photoIds'=>array_values($arIds),
            'newModelId'=>$model,
            'layer'=>$layer
        ];
    }
}