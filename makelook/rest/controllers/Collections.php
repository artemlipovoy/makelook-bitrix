<?php


namespace MakeLook\Rest\Controllers;

use MakeLook\Rest\EntityInterface;
use Prominado\Rest\Request;
use MakeLook\Entities\Clothes\{
    SizesTable,
    CompositionTable
};

class Collections extends Base
{

    public function getClothesCollections(){
        $req = new Request();
        $req->setQuery([
            '__class'=>"\MakeLook\Entities\Clothes\SizesTable",
        ]);
        $sizes = EntityInterface::restGetList($req);
        $clothesSizes = [];
        foreach($sizes as $v){
            $clothesSizes[$v['type']][] = $v;
        }
        $req = new Request();
        $req->setQuery([
            '__class'=>"\MakeLook\Entities\Clothes\CompositionTable",
        ]);
        $clothesCompositions = EntityInterface::restGetList($req);

        $req = new Request();
        $req->setQuery([
            '__class'=>"\MakeLook\Entities\ModelsTable",
        ]);
        $models = EntityInterface::restGetList($req);

        $req = new Request();
        $req->setQuery([
            '__class'=>"\MakeLook\Entities\ColorsTable",
        ]);
        $colors = EntityInterface::restGetList($req);

        return [
            'sizes'=>$clothesSizes,
            'compositions'=>$clothesCompositions,
            'models'=>$models,
            'colors'=>$colors
        ];
    }
}