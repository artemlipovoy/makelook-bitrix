<?php


namespace MakeLook\Rest\Controllers\Integration;


use MakeLook\Rest\Controllers\Base;
use MakeLook\Rest\Services\Bitrix\CRest;
use MakeLook\Rest\Services\Bitrix\Entities\CrmContact;
use MakeLook\Rest\Services\Bitrix\Entities\CrmDeal;
use MakeLook\Rest\Services\Bitrix\Entities\CrmProduct;
use MakeLook\Telegram\Connector;
use Prominado\Rest\Request;

class Bitrix24 extends Base
{
    /**
     * Request['products'] - ID товаров в заказе
     * Request['opportunity'] - Сумма заказа
     * Request['contact']['name'] - Имя клиента
     * Request['contact']['phone'] - Телефон клиента
     * Request['contact']['addr'] - Адрес доставки
     *
     * @param Request $request
     * @return array|bool
     */
    public static function createOrder(Request $request){
        $contactFields = $request->getQuery('contact');
        $dealId = CrmDeal::add([
            'TITLE'=>"Заказ {$contactFields['phone']}",
            'OPPORTUNITY'=>$request->getQuery('opportunity'),
            'COMMENTS'=>"Адрес: {$contactFields['addr']}",
        ]);
        if($dealId){
            $duplicates = CRest::call('crm.duplicate.findbycomm',[
                'entity_type'=>'CONTACT',
                'type'=>'PHONE',
                'values'=>[$contactFields['phone']]
            ])['result'];
            $contactId = $duplicates['CONTACT'][0];
            if(!$contactId){
                $contactId = CrmContact::add([
                    'NAME'=>$contactFields['name'],
                    'OPENED'=> "Y",
                    'TYPE_ID'=> "CLIENT",
                    'SOURCE_ID'=> "SELF",
                    'PHONE'=>[
                        [
                            'VALUE'=>$contactFields['phone'],
                            'VALUE_TYPE'=>'WORK'
                        ]
                    ]
                ]);
            }
            if($contactId){
                CRest::call('crm.deal.contact.add',[
                    'id'=>$dealId,
                    'fields'=>['CONTACT_ID'=>$contactId]
                ]);
            }

            foreach($request->getQuery('products') as $product){
                $productId = CrmProduct::add([
                    'XML_ID'=>$product['product']['ID'],
                    'NAME'=>$product['product']['name'],
                    'CURRENCY_ID'=>'RUB',
                    'PRICE'=>$product['product']['price'],
                    'MEASURE'=>CrmProduct::TYPE_PIECE,
                    'PROPERTY_100'=>[
                        "fileData"=>[
                            "1.jpg",
                            explode(',',is_array($product['product']['photo'])?$product['product']['photo'][0]:$product['product']['photo'])[1]
                        ]
                    ]
                ])['result'];

                $arProductRows[] = [
                    "PRODUCT_ID"=>$productId,
                    "PRODUCT_NAME"=>$product['product']['name'].", размер: ".$product['size']['size'],
                    "QUANTITY"=>$product['quantity'],
                    "PRICE"=>$product['product']['price']
                ];
            }
            if($arProductRows) {
                CRest::call("crm.deal.productrows.set", [
                    'id' => $dealId,
                    'rows' => $arProductRows
                ]);
            }
            if(\Bitrix\Main\Loader::includeModule('telegram')) {
                Connector::sendMessage('ADMIN', "
🎉Новый заказ

👤 {$contactFields['name']}
📱 {$contactFields['phone']}
📍 {$contactFields['addr']}

Ссылка на сделку: https://makelook.bitrix24.ru/crm/deal/details/{$dealId}/
            ");
            }
            return [
                'dealId'=>$dealId,
                'contactId'=>$contactId
            ];
        }
        return false;
    }
}