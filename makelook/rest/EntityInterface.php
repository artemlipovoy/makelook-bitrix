<?php


namespace MakeLook\Rest;


use Bitrix\Main\{ArgumentException, Entity\ExpressionField, FileTable, ObjectPropertyException, SystemException};
use Prominado\{Rest\Request, Rest\RestException};

class EntityInterface
{
    /**
     * @param Request $request
     * @return array
     * @throws ArgumentException
     * @throws ObjectPropertyException
     * @throws SystemException
     */
    public static function restGetList(Request $request){
        $class = $request->getQuery('__class');
        if(!$class)
            throw new RestException('Wrong class');

        $aliases = $class::getRestAliases();

        //Если указан фильтр - заменяем алиасы на реальные названия
        if($request->getQuery('filter')){
            foreach($request->getQuery('filter') as $filterKey=>$filterValue){
                if(in_array($filterKey,array_keys($aliases))) {
                    $arFilter['filter'][$aliases[$filterKey]] = $filterValue;
                }else{
                    throw new RestException("`{$filterKey}` field is incorrect for {$class}");
                }
            }
        }

        //Если указаны выбранные поля - заменяем алиасы на реальные значения
        if($request->getQuery('select')){
            foreach($request->getQuery('select') as $selectValue){
                if(in_array($selectValue,array_keys($aliases))) {
                    $arFilter['select'][$selectValue]=$aliases[$selectValue];
                }else{
                    throw new RestException("`{$selectValue}` field is incorrect for {$class}");
                }
            }
        }else{
            $arFilter['select'] = $aliases;
        }

        if($limit = $request->getQuery('limit'))
            $arFilter['limit'] = $limit;
        if($arFilter)
            $response = $class::getList($arFilter)->fetchAll();
        else
            $response = $class::getList()->fetchAll();

        $response = self::joinEntities($class,$response);
        //В случае если запрос происходит по одному идентификатору - отдаем единственное значение
        if($request->getQuery('__other')=='byId')
            return array_pop($response);

        return $response;
    }

    /**
     * @param Request $request
     * @return array|bool|false
     * @throws ArgumentException
     * @throws ObjectPropertyException
     * @throws RestException
     * @throws SystemException
     */
    public static function restGetById(Request $request){
        $class = $request->getQuery('__class');
        if(!$class)
            throw new RestException('Wrong class');
        if(!$request->getQuery('id'))
            throw new RestException('Empty document id');

        //Генерируем фильтр для поиска по ID
        $fakeRequest = new Request();
        $fakeRequest->setQuery([
            'filter'=>[
                'ID'=>$request->getQuery('id'),
            ],
            '__class'=>$request->getQuery('__class'),
            '__other'=>'byId'
        ]);
        return self::restGetList($fakeRequest);
    }

    /**
     * @param $fields
     * @return array
     */
    private static function joinEntities($class, $fields){
        foreach($fields as $k=>$element){
            $fields[$k] = self::getEntityData($class, $element);
        }
        return $fields;
    }

    /**
     * @param $class
     * @param $result
     * @return mixed
     * @throws ArgumentException
     * @throws ObjectPropertyException
     * @throws SystemException
     */
    private static function getEntityData($class, $result){
        foreach($result as $key=>$value){
            $realKey = self::getRealKey($class, $key);
            $fieldData = $class::getMap();
            switch($fieldData[$realKey]['type']){
                //Для поля файл - заменям идентификатор на публичную ссылку
                case $class::FIELD_FILE:
                    $res = FileTable::getList([
                        'filter'=>['ID'=>$value],
                        'select'=>[
                            new ExpressionField(
                                'FILE_URL',
                                'CONCAT("https://'.$_SERVER['HTTP_HOST'].'/upload/",%s,"/",%s)',
                                ['SUBDIR', 'FILE_NAME']
                            )
                        ]
                    ])->fetchAll();
                    if(count($res)>1){
                        $results = [];
                        foreach ($res as $v)
                            $results[] = $v['FILE_URL'];
                        $result[$key] = $results;
                    }else{
                        $result[$key] = $res[0]['FILE_URL'];
                    }
                    break;
                //Для поля картинки - заменям идентификатор на base64
                case $class::FIELD_IMAGE:
                    $res = FileTable::getList([
                        'filter'=>['ID'=>$value],
                        'select'=>[
                            new ExpressionField(
                                'FILE_URL',
                                'CONCAT("/upload/",%s,"/",%s)',
                                ['SUBDIR', 'FILE_NAME']
                            )
                        ]
                    ])->fetchAll();
                    if(count($res)>1){
                        $results = [];
                        foreach ($res as $v) {
                            $type = pathinfo($_SERVER['DOCUMENT_ROOT'].$v['FILE_URL'], PATHINFO_EXTENSION);
                            $data = file_get_contents($_SERVER['DOCUMENT_ROOT'].$v['FILE_URL']);
                            $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
                            $results[] = $base64;
                        }
                        $result[$key] = $results;
                    }else{
                        $type = pathinfo($_SERVER['DOCUMENT_ROOT'].$res[0]['FILE_URL'], PATHINFO_EXTENSION);
                        $data = file_get_contents($_SERVER['DOCUMENT_ROOT'].$res[0]['FILE_URL']);
                        $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
                        $result[$key] = $base64;
                    }
                    break;
                //Для поля объект - заменяем идентификаторы на поля объекта
                case $class::FIELD_ENTITY:
                    $fakeRequest = new Request();
                    $fakeRequest->setQuery([
                        'filter'=>['ID'=>$value],
                        '__class'=>$fieldData[$realKey]['class']
                    ]);
                    $result[$key] = self::restGetList($fakeRequest);
                    break;
                //Для поля типа список - заменяем идентификаторы на значения из values
                case $class::FIELD_LIST:
                    $values = $fieldData[$realKey]['values'];
                    $result[$key] = $values[$value];
                    break;
                //Для поля типа функция - вызываем указанную функцию класса из function
                case $class::FIELD_FUNCTION:
                    $result[$key] = call_user_func($class.'::'.$fieldData[$realKey]['function'], $result);
                    break;
                default:
                    break;
            }
        }
        return $result;
    }

    private static function getRealKey($class, $key){
        return $class::getRestAliases()[$key];
    }
}