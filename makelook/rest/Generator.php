<?php


namespace MakeLook\Rest;

use Bitrix\Main\EventManager;
use MakeLook\Rest\Controllers\Clothes;
use MakeLook\Rest\Controllers\Collections;
use MakeLook\Rest\Controllers\Integration\Bitrix24;

class Generator
{
    const REST_CLASSES = [
        "\MakeLook\Entities\ModelsTable",
        "\MakeLook\Entities\BrandsTable",
        "\MakeLook\Entities\ColorsTable",
        "\MakeLook\Entities\Clothes\SizesTable",
        "\MakeLook\Entities\Clothes\CompositionTable",
        "\MakeLook\Entities\Clothes\CategoriesTable",
        "\MakeLook\Entities\Clothes\ClothesTable",
        "\MakeLook\Entities\Clothes\PhotosTable",
    ];

    /**
     * @return array
     */
    public static function get(){
        $restMap = [];
        foreach(self::REST_CLASSES as $class){
            $re = '/MakeLook\\\\Entities\\\\(.*)Table/m';
            preg_match($re, $class, $matches);
            $restEntity = strtolower(str_replace('\\','.',$matches[1]));
            $restMap[$restEntity.'.get'] = [
                'allow_methods'=>['GET'],
                'callback'=>['\MakeLook\Rest\EntityInterface', 'restGetById'],
                'class'=>$class
            ];
            $restMap[$restEntity.'.list'] = [
                'allow_methods'=>['GET'],
                'callback'=>['\MakeLook\Rest\EntityInterface', 'restGetList'],
                'class'=>$class
            ];
        }
        $restMap = array_merge($restMap, (new Clothes)->getMap());
        $restMap = array_merge($restMap, (new Collections)->getMap());
        $restMap = array_merge($restMap, (new Bitrix24)->getMap());
        return $restMap;
    }


    public static function bind(){
        EventManager::getInstance()->addEventHandler(
            'prominado.rest',
            'onRestMethodBuildDescription',
            [
                '\MakeLook\Rest\Generator',
                'get'
            ]
        );
    }
}