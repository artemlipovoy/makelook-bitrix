<?php
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

//_______
$TABLE_NAME = "ml_clothes_categories";
//_______

$connection = Bitrix\Main\Application::getConnection();
$sqlHelper = $connection->getSqlHelper();
$columns = $connection->query("DESCRIBE {$TABLE_NAME}");

$map = [];
$aliases = [];
while($column = $columns->fetch()){
    $map[$column['Field']] = [
        'type' => $column['Key']=="PRI"?"primary":$column['Type']
    ];
    if($column['Field']=='ID'){
        $alias = 'ID';
    }else{
        $alias = str_replace('uf_', '', strtolower($column['Field']));
    }
    $aliases[$alias] = $column['Field'];
}

echo("<pre>");
echo("public static function getMap(){
");
echo("return ");var_export($map);echo(";
");
echo("}
");
echo("public static function getRestAliases(){
");
echo("return ");var_export($aliases);echo(";
");
echo("}
");
echo("</pre>");